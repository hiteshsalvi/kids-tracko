// roles should be an array of ADMIN , PARENT

const apiHelper = require('../helpers/api.helper');

const validateRoles = roles => {
  return (req, res, next) => {
    if ( roles.includes(req.user.role) ) {
      return next();
    } else {
      return apiHelper.failure(res, ['Not allowed to access requested resource.'], 'ACCESS_VALIDATION', 403);
    }
  };
};

module.exports = {
  validateRoles: validateRoles,
  all: validateRoles(['ADMIN', 'PARENT']),
  admins: validateRoles(['ADMIN'])
};
