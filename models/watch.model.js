const mongoose = require('mongoose');
const MODELS = require('../constants/models.constants');

const WatchSchema = new mongoose.Schema({
  isProcured: {
    type: Boolean,
    default: false,
    required: true,
  },
  isValid: {
    type: Boolean,
    default: true,
    required: true,
  }
}, { timestamps: true });

// WatchSchema.index({ isProcured: 'text', isValid: 'text' });

mongoose.model(MODELS.Watch, WatchSchema);
